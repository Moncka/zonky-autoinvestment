# zonky-autoinvestment

## root and admin iam user setup when AWS account is created
ref: https://blog.gruntwork.io/a-comprehensive-guide-to-authenticating-to-aws-on-the-command-line-63656a686799

result: admin user is created

## setup directory
Provisions the state in AWS account for cooperation purposes and creates the terraform user (locally by admin user).

see [setup/README.md](setup/README.md)

---
## Other useful references:

### local setup of Gitlab runner
https://docs.gitlab.com/runner/install/docker.html

### Gitlab CI with Terraform
https://matavelli.io/posts/2020/01/setup-gitlab-ci-with-terraform/

### policy generator
https://awspolicygen.s3.amazonaws.com/policygen.html
