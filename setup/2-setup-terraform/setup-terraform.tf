resource "aws_iam_user" "terraform" {
  name = "terraform"
  tags = {
    tag-key = "terraform"
  }
}

resource "aws_iam_user_policy" "terraform" {
  name = "terraform-policy"
  user = "${aws_iam_user.terraform.name}"

  policy = jsonencode(
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Action": "iam:Get*",
          "Resource": "*"
        }
      ]
    }
  )
}

resource "aws_iam_access_key" "terraform" {
  user    = "${aws_iam_user.terraform.name}"
  pgp_key = "keybase:${var.keybase_user}"
}
