variable "account_id" {
  default = "965568659521"
}

variable "keybase_user" {
  description = "A keybase username to encrypt the secret key output."
  default     = "davidmoncka"
}
