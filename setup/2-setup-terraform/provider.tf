terraform {
  required_version = ">= 0.12"

  backend "s3" {
    key            = "account.tfstate"
    bucket         = "965568659521-terraform-state-01"
    dynamodb_table = "965568659521-terraform-state-01"
    acl            = "bucket-owner-full-control"
    region         = "eu-west-1"
  }
}

provider "aws" {
  version                 = ">= 2.27"
  region                  = "eu-west-1"
}
