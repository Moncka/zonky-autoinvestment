output "access_key" {
  value = aws_iam_access_key.terraform.id
}

output "secret_access_key" {
  value = aws_iam_access_key.terraform.encrypted_secret
}
