# setup terraform

## create table "lock" in dynamo db + file "state" in s3 bucket
directory: 1-setup-state

## terraform IAM user and terraform state in s3 bucket + lock in dynamo db
directory: 2-setup-terraform

When state is in s3 and dynamodb, state is referenced in provider.tf - terraform > backend > s3.


```bash
terraform plan
terraform apply
terraform output secret_access_key | base64 --decode | keybase pgp decrypt
```

ref: https://nextlinklabs.com/insights/building-cicd-pipeline-website-gitlab

---
### run terraform in docker
```bash
docker run -v `pwd`:/workspace -w /workspace -e AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY hashicorp/terraform init
docker run -v `pwd`:/workspace -w /workspace -e AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY hashicorp/terraform plan
docker run -v `pwd`:/workspace -w /workspace -e AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY hashicorp/terraform apply
```
