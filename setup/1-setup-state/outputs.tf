output "bucket_id" {
  description = "The name of the bucket."
  value       = aws_s3_bucket.state.id
}

output "table_id" {
  description = "The name of the table."
  value       = aws_dynamodb_table.lock.id
}
