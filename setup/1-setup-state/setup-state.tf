resource "aws_s3_bucket" "state" {
  bucket = "${var.account_id}-terraform-state-${var.name_postfix}"
  acl    = "private"

  versioning {
    enabled = true
  }

  tags = {
    "Name" = "${var.account_id}-terraform-state"
  }

}

resource "aws_dynamodb_table" "lock" {
  name           = "${var.account_id}-terraform-state-${var.name_postfix}"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    "Name" = "${var.account_id}-terraform-state"
  }
}
